# AdminButtons
Read button names on maps in source games and give admins access to them

## Description

I made this plugin with Zombie Escape in mind.
A lot of ZE maps have admin rooms that let you change levels and its rather annoying to have to noclip/tp to the buttons and then try to find the right button.
This plugin looks at all the func_buttons on the map with certain names and adds them to a menu admins can use to use the button.

## Features
The plugin automatically makes a list of all the buttons with substrings found in the admin_buttons.txt file.
The plugin allows viewing of the buttons that are in the list accessable to admins, and a list of all the maps buttons.
The plugin allows root admins to temporarily add other buttons in the map to the list of accessable buttons with a simple command.

## Commands
- sm_buttonwatchlist: List all phrases flagged by the Admin Buttons plugin
- sm_buttonlist: List all button names on the map
- sm_buttons: Allows admin to activate any buttons on the map (Reserved for generic admins and higher)
- sm_buttonadd <arg>: Allows root admin to add a search term and redo the button search for that term (Reserved for root admins) (eg. "/buttonadd bubble")